#include <string.h>  // used for serial communication
#include <Wire.h>    // used for I2C communication
#include "src/Si7021Readout.h"
#include "src/KIRAControl.h"

#define SDA 2 // I2C data
#define SCL 3 // I2C clock

// Serial communication
String input = "";
char serialBuffer[256];

// Class instances for temperature sensor and KIRA control
Si7021Readout fSensor = Si7021Readout();
KIRAControl fKIRA = KIRAControl();
bool fTriggerState = false;
long count = 0;
long fDuration = 0;
// Arduino setup method
void setup() {
    Serial.begin(9600);  // Setup serial communication
    while (!Serial) {
        // wait for serial port to connect.
    }

    pinMode(LED_BUILTIN_TX,INPUT); // Switch off TX LED
    pinMode(LED_BUILTIN_RX,INPUT); // Switch off RX LED

    fKIRA.init();
}


void loop() {
    input = "";
    // Check for serial input
    if (Serial.available() > 0) {
        input = Serial.readStringUntil('\n');
        // T and RH sensor readout
        if (input.startsWith("version"))
        {
            Serial.println("2.1");
        }
        else if (input.startsWith("temperature"))
        {
            Serial.println(fSensor.read_temperature());
        }
        else if (input.startsWith("humidity")) {
            Serial.println(fSensor.read_humidity());
        }
        else if (input.startsWith("dewpoint")) {
            Serial.println(fSensor.calculate_dewpoint());
        }
        // set DAC output gain multiplier
        else if (input.startsWith("dacLed_high")) 
        {
             fKIRA.setDACGain(1);
             //Serial.println("DAC reference multiplier is set to: 2 x Vref");
        }
        else if (input.startsWith("dacLed_low"))
        {
            fKIRA.setDACGain(0);
            //Serial.println("DAC reference multiplier is set to: 1 x Vref");
        }
        // set LED Intensities
        else if (input.startsWith("intensity_")) 
        {
            int cLED = input.substring(10,12).toInt();
            int cIntensity = input.substring(13,25).toInt();
            //Serial.print("Set LED ");
            //Serial.print(cLED);
            //Serial.print(" to Intensity ");
            //Serial.println(cIntensity);
            fKIRA.setLEDIntensity(cLED, cIntensity);
        }
        else if (input.startsWith("UpdateDAC")) 
        {
            fKIRA.updateDAC();
        }
        else if (input.startsWith("ResetDAC"))
        {
            fKIRA.resetDAC();
        }
        else if (input.startsWith("pulseLength_"))
        {
            unsigned long cPL = input.substring(12,17).toInt();
            fKIRA.setRheostat(cPL);
        }
        else if (input.startsWith("frequency_"))
        {
            unsigned long freq = atol( input.substring(10, 25).c_str() );
            fKIRA.setFrequency(freq);
            //Serial.print("Set Frequency ");
            //Serial.println(corr_freq);
        }
        else if (input.startsWith("trigger_on"))
        {
            fTriggerState = true;
            //Serial.println("Activate Triggers");
        }
        else if (input.startsWith("trigger_off"))
        {
            fTriggerState = false;
            //Serial.println("Deactivate triggers");
        }
        else if (input.startsWith("highRateBurst_"))
        {
            unsigned long nBurst = atol( input.substring(14, 29).c_str() ) ;
            fKIRA.highRateBurst(nBurst);
        }
        Serial.flush();
    }
    if (fTriggerState)
    {
      fKIRA.generatePulse();
    }
}
 
