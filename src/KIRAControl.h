#include <Systronix_AD5274.h>   // library for AD5274 digital rheostat
#include <Wire.h>

class KIRAControl {
    public:
        KIRAControl();
        ~KIRAControl();
        void init();
        int setDACGain(int pGain);
        void setRheostat(int pResistance);
        void setLEDIntensity(int pLED, int pIntensity);
        void generatePulse();
        void updateDAC();
        void resetDAC();
        unsigned long setRheostat(unsigned long pResistance);
        void setFrequency(unsigned long pFrequency);
        void highRateBurst(unsigned long nBurst);

    private:
        // Pin assignments
        const int fPinPulseGeneration = 4;  // pin to create us pulse
        const int fPinDACReset = 12;        // pin to reset DAC on twin boards
        const int fPinLDAC = 9;             // LDAC pin of DAC on twin boards
        const int fPinDACGain = 10;         // pin to set DAC output gain
        const int fPinErrorIC1 = 11;        // ERROR pin for LED driver IC1
        const int fPinErrorIC2 = 8;         // ERROR pin for LED driver IC2

        // I2C adresses
        const int fAdrDACTop = 12;          // I2C address of DAC on top twin board
        const int fAdrDACBot = 14;          // I2C address of DAC on bottom twin board
        Systronix_AD5274 fRheostat = Systronix_AD5274(0x2E); //define the AD5272/74 with I2C address

        // us Pulse settings
        unsigned long fFrequency = 1000;       // trigger frequency in Hz (max 8kHz)
        double fDuration = ( 1.0 /  ( (double) fFrequency ) ) * 1000000 ;

        void I2CWrite(int pAddress, byte pInstruction, int pValue);
        void setDACChannel(int pChannelNumber, int pValue);
        unsigned long frequencyLUT( unsigned long pFrequency );
};
