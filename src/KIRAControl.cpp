#include "KIRAControl.h"

KIRAControl::KIRAControl() {}

KIRAControl::~KIRAControl() {}

void KIRAControl::init() {
    Wire.begin();   // join i2c bus (address optional for master)
    
    // Initialise pins as inputs and outputs
    pinMode(fPinErrorIC1, INPUT);
    pinMode(fPinErrorIC2, INPUT);
    pinMode(fPinPulseGeneration, OUTPUT);
    pinMode(fPinDACGain, OUTPUT);
    pinMode(fPinLDAC, OUTPUT);
    pinMode(fPinDACReset, OUTPUT);

    // Initialise pins at default state
    digitalWrite(fPinDACGain, LOW);     // set gain of DAC outputs: 1x or 2x Vref
    digitalWrite(fPinLDAC, HIGH);
    digitalWrite(fPinDACReset, HIGH);
}

int KIRAControl::setDACGain(int pGain)
{
    if (pGain == HIGH) {
        digitalWrite(fPinDACGain, HIGH);
        return 1;
    }
    digitalWrite(fPinDACGain, LOW);
    return 0;
}

void KIRAControl::I2CWrite(int pAddress, byte pInstruction, int pValue)
{
    Wire.beginTransmission(pAddress);   // transmit to device with address given in datasheet
    Wire.write(pInstruction);           // sends instruction byte  
    Wire.write(highByte(pValue));       // sends potentiometer value byte 
    Wire.write(lowByte(pValue));        // sends potentiometer value byte
    Wire.endTransmission();             // stop transmitting
}

void KIRAControl::updateDAC() {
    // take the LDAC pin LOW to update DAC registers as long as input registers have new data:
    digitalWrite(fPinLDAC, LOW);
    delay(100);
    // return to default state of LDAc pin:
    digitalWrite(fPinLDAC, HIGH);
}

void KIRAControl::resetDAC() {
    // take the LDAC pin LOW to update DAC registers as long as input registers have new data:
    digitalWrite(fPinDACReset, LOW);
    delay(100);
    // return to default state of LDAc pin:
    digitalWrite(fPinDACReset, HIGH);
}

void KIRAControl::setDACChannel(int pChannelNumber, int pValue) {
    int cAddress = fAdrDACTop;
    if (pChannelNumber > 7)
    {
        cAddress = fAdrDACBot;
        pChannelNumber = pChannelNumber - 8;
    }
    byte cChannelArray[8] = {B00000000, B00000001, B00000010, B00000011, B00000100, B00000101, B00000110, B00000111};
    byte cCommand = B00000011;
    byte cInstruction = (cCommand << 4) + cChannelArray[pChannelNumber];
    // Serial.println(instruction, BIN);
    I2CWrite(cAddress, cInstruction, pValue);
}

void KIRAControl::setLEDIntensity(int pLED, int pIntensity)
{
    // Correct routing error on twin board circuits
    if (pLED == 3) pLED = 2;
    else if (pLED == 2) pLED = 3;
    else if (pLED == 4) pLED = 5;
    else if (pLED == 5) pLED = 4;
    else if (pLED == 10) pLED = 11;
    else if (pLED == 11) pLED = 10;
    else if (pLED == 12) pLED = 13;
    else if (pLED == 13) pLED = 12;
    if (pIntensity > 65536) pIntensity = 0;
    setDACChannel(pLED, pIntensity);
}

// TODO: Dewpoint
void KIRAControl::generatePulse()
{
  digitalWrite(4, HIGH);
  digitalWrite(4, LOW);
  delayMicroseconds( (int) ( fDuration ) );
}

unsigned long KIRAControl::setRheostat(unsigned long pPulseLength)
{
    //Convert ns pulse length in DAC Value with calibration function:
    //3.142e-09 x³ - 1.41e-05 x² + 0.1618 x - 7.601

    float dac_precise = 3.142*1e-9*pPulseLength*pPulseLength*pPulseLength - 1.41*1e-9*pPulseLength*pPulseLength + 0.1618 * pPulseLength -7.601;
    int dac = (int) (dac_precise +  0.5);
    if (dac < 0)
    {
        dac = 0;
    }
    if (dac > 1023)
    {
        dac = 1023;
    }

    unsigned long resistance = dac;
    fRheostat.begin();
    fRheostat.control_write_verified(0x00);            
    fRheostat.control_write_verified(AD5274_RDAC_WIPER_WRITE_ENABLE);
    
    // unsigned long cValue = 1023;                // range of value 0 - 1023 (only Integer)

    // value = 1024 * res_value / 100000;             // calc from OHM-value to a decimal number  
    // if(value > 1023) value = 1023;                 // maximum limit

    Systronix_AD5274 unlock(true);                       // unlock the resistor for config 
    fRheostat.command_write(0x01, resistance);                      // set the value of the resistor
    return  fRheostat.command_read(0x02, 0x00);        // read the value for checking the resisitor                                       
}

void KIRAControl::setFrequency(unsigned long  pFrequency)
{
    //fFrequency = pFrequency;
    fFrequency = frequencyLUT ( pFrequency ) ;
    fDuration = ( 1.0 /  ( (double) fFrequency ) ) * ( 1000000 );
}

void KIRAControl::highRateBurst(unsigned long nBurst)
{
  for (unsigned long i = 0; i < nBurst; i++)
  {
    digitalWrite(4, HIGH);
    digitalWrite(4, LOW);
  }
}

unsigned long KIRAControl::frequencyLUT(unsigned long pFrequency)
{
    unsigned long x [20][2] = {
        {0,     90},
        {90,    175},
        {175,   260},
        {260,   440},
        {440,   860},
        {860,   1650},
        {1650,  2450},
        {2450,  3950},
        {3950,  7200},
        {7200,  12100},
        {12100, 15900},
        {15900, 20500},
        {20500, 23750},
        {23750, 26650},
        {26650, 31050},
        {31050, 32900},
        {32900, 34200},
        {34200, 35150},
        {35150, 40850},
        {40850, 10000000}
    };


    unsigned long y [20][2] = {
        {0,         100},
        {100,       200},
        {200,       300},
        {300,       500},
        {500,       1000},
        {1000,      2000},
        {2000,      3000},
        {3000,      5000},
        {5000,      10000},
        {10000,     20000},
        {20000,     30000},
        {30000,     50000},
        {50000,     70000},
        {70000,     100000},
        {100000,    200000},
        {200000,    300000},
        {300000,    500000},
        {500000,    1000000},
        {1000000,   10000000},
        {10000000,  100000000}
    };

    unsigned long returnValue = 1000;

    for (int i = 0; i < 20; i++)
    {
        if (x[i][0] <= pFrequency && pFrequency < x[i][1])
        {
            returnValue = (unsigned long) ( (y[i][1] - y[i][0] ) / ( x[i][1] - x[i][0] ) * ( pFrequency - x[i][0] ) + y[i][0] );
        }
    }
    return returnValue;
}


