#include "Si7021Readout.h"

Si7021Readout::Si7021Readout() {
    fSensor = Adafruit_Si7021();
    fNoValue = 999.;
}

Si7021Readout::~Si7021Readout() {}

double Si7021Readout::read_temperature()
{
    if (fSensor.begin()) {
        return fSensor.readTemperature();
    }
    return fNoValue;
}

double Si7021Readout::read_humidity()
{
    if (fSensor.begin()) {
        double humidity = fSensor.readHumidity();
        humidity += 2.1;
        if (humidity < 0)
        {
            return 0.1;
        }
        else{
            return humidity;
        }
    }
    return fNoValue;
}

double Si7021Readout::calculate_dewpoint()
{
    double cTemp = read_temperature();
    double cRH = read_humidity();
    if (cTemp != fNoValue && cRH != fNoValue)
    {
        return (241.2*log(cRH/100.) + (4222.03716*cTemp)/241.2+cTemp)/(17.5043-log(cRH/100.)-(17.5043*cTemp)/(241.2+cTemp));
    }
}